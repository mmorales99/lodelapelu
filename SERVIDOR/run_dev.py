import uvicorn
import os

if __name__ == "__main__":
    uvicorn.run("main:app",
                host="0.0.0.0",
                port=12345,
                app_dir=os.path.join(".","SRC"),
                reload=True,
                reload_dirs=os.path.join(".","SRC"),
                debug=True,
                server_header=False,
                date_header=False,
                )

