import time
from fastapi import FastAPI, Request
from webservice import app as webserviceapp
# from usuariosservice import app as usuariosservice

app = FastAPI()

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response

app.mount('/',webserviceapp)
# app.mount('/usuarios',usuariosservice)








# @app.exception_handler(RequestValidationError)
# async def validation_exception_handler(request, exc:RequestValidationError):
#     print(str(exc))
#     return StandardException.genResponse(StandardException('Ups! Algunos datos se han corrompido! Pondremos a nuestros pelusos a trabar en ello.'))

# @app.exception_handler(Exception)
# async def validation_exception_handler(request, exc:Exception):
#     print(str(exc))
#     return StandardException.genResponse(StandardException('Ha habido un error desconocido, nuestras pelusas están trabajando en ello'))


###################################################################
# @app.post('/repository/{repository:path}')
# async def repository_options(repository:str):
#     return Response(status_code=HTTPStatus.OK)



# class version(Resource):
#     def get(self):
#         return getFromConfigFile('version')
#     def post(self):
#         data = request.get_json()
#         # hay nueva version publica?
#         return '{"token":"NuevoToken", "comprimido":"zip"}'
#         # parser = reqparse.RequestParser()
#         # parser.add_argument('token',required=True)
#         # parser.add_argument('ea',required=False)
#         # args = parser.parse_args()
#         # debería mirar si el token esta registrado en la bd
#         # if args['ea'] is not None:
#         #     return "{'token':'NuevoToken', 'comprimido':'zip'}"
#         # else:
#         #     # hay nueva version publica?
#         #         return "{'token':'NuevoToken', 'comprimido':'zip'}"

# api.add_resource(version,'/repository/version')

# class usuarios(Resource):
#     def get(self):
#         return "{'Usuarios':'No hay'}"
#     pass
# api.add_resource(usuarios,'/usuarios')

# if __name__ == '__main__':
#     app.run()