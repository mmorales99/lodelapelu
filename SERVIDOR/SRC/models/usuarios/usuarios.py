from hashlib import new
from uuid import UUID
from venv import create
from pydantic import BaseModel, Field

#     self.id = UUID(new('sha1', bytes(usuario.nombre, 'utf-8')).hexdigest())
class Usuario(BaseModel):
    id:UUID|None = None
    nombre:str|None = None
    contraseña:str|None = None
    email:str|None = None
    newsletter:bool|None = None

    # def create(self, usuario:'Usuario'):
    #     self.nombre = usuario.nombre
    #     self.contraseña = usuario.contraseña
    #     self.email = usuario.email
    #     return self