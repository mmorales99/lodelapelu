import uvicorn
import os

route = os.path.join('SRC','services','web')
module = 'webservice:app'

if __name__ == "__main__":
    uvicorn.run(module,
                host="0.0.0.0",
                port=12345,
                app_dir=route,
                reload=True,
                reload_dirs=route,
                debug=True,
                server_header=False,
                date_header=False,
                )