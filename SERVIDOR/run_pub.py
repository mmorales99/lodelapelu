import uvicorn
import os

if __name__ == "__main__":
    uvicorn.run("main:app",
                host="0.0.0.0",
                port=80,
                workers=1,
                app_dir=os.path.join(".","SRC"),
                server_header=False,
                date_header=False,
                # ssl-keyfile=os.path.join('.','cert','key.pem')
                # ssl-certfile=os.path.join('.','cert','cert.pem')
                )
