from sys import argv
from os.path import join
from PyInstaller import __main__ as pyinstaller

project: dict = {
    'SERVIDOR':'.\\SERVIDOR\\SRC\\',
    'WEB':'.\\WEB\\SRC',
    'TINTRACKER':'.\\TINTRACKER\\V2\\SRC\\'
}

action: dict = {
    'DEV':'develop', # development build
    'REV':'review', # review build
    'TEST':'test',# build tests
    'PRO':'production', # production build
    'PUB':'publish'  # publish changes
}

if len(argv) == 2 and ('h' in argv):
    print("""
        -------------------------------------------------------------
        Usage: build [DEV|REV|TEST|PRO|PUB] [SERVIDOR|WEB|TINTRACKER|YOUR_OWN_ROUTE]
        -------------------------------------------------------------
        'DEV'  ->  development build
        'REV'  ->  review build
        'TEST' ->  build tests
        'PRO'  ->  production build
        'PUB'  ->  publish changes
        -------------------------------------------------------------
        'SERVIDOR':'.\\SERVIDOR\\SRC\\',
        'WEB':'.\\WEB\\SRC',
        'TINTRACKER':'.\\TINTRACKER\\V2\\SRC\\'
    """)
else:
    build_file = action[argv[1]]+'_build.spec'
    build_dir = join(project[argv[2]],'build_specs\\')
    pyinstaller.run_build(spec_file=join(build_dir,build_file))
    