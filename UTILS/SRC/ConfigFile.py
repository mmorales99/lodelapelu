TTVERSION = '2022.05.07.02.10'
configFilename = '.cnfg'1510

def create():
    with open(file=configFilename,mode='w') as f:
        f.write(f'version:{TTVERSION}')
    
def get(token):
    try:
        with open(file=configFilename,mode='r') as f:
            line = list(filter(lambda x: token in str(x).lower(), f.readlines()))[0]
            i = line.find(':',0)
            return line[i+1:]
    except FileNotFoundError:
        create()
        return get(token=token)