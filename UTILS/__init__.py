from .SRC.ConfigFile import get as getFromConfigFile
from .SRC.ConfigFile import *

__all__ = [
    'getFromConfigFile',
]