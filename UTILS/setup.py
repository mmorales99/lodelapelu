from setuptools import setup

from SRC import __version__

setup(
    name='Utils',
    version=__version__,
    description='Paquete de utilidad y soporte para operaciones de los proyectos TT',
    url='https://gitlab.com/mmorales99/lodelapelu/-/tree/master/UTILS',
    author='Manuel Juan Morales Amat',
    author_email='mjmorales.mcv@gmail.com',
    # license='BSD 2-clause',
    packages=['SRC'],
    install_requires=['',],
    classifiers=[
        'Development Status :: 1 - Planning',
    ],
)