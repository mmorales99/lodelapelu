from sys import argv
from os.path import join
from os import mkdir

page_name = argv[1]

try:
    mkdir(join('SRC',page_name))
except: pass
try:
    with (open(join('SRC',page_name,page_name+'.html'),'w')) as f:
        f.write('<link rel="stylesheet" href="'+page_name+'.css" />\n')
        f.write('<script src="'+page_name+'"></script>\n')
        f.write('<section id="'+page_name+'">\n')
        f.write('</section>')
except: pass

try:
    with (open(join('SRC',page_name,page_name+'.css'),'w')) as f:
        f.write('/* CSS de la pagina */\n')
        f.write('#'+page_name+' {padding: 10vh;}')
except: pass

try:
    with (open(join('SRC',page_name,page_name+'.js'),'w')) as f:
        f.write('/* JS de la pagina */')
except: pass

