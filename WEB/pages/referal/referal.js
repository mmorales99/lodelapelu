/* JS de la pagina */
$((e)=>{
    hideHeader();
    emailValidation();
    var suscribirse = true;
    $('#suscribirse').click((e)=>{
        debugger;
        e.preventDefault();
        suscribirse = !suscribirse;
        if(suscribirse){
            $('#suscribirse').removeClass('check false');
            $('#suscribirse').addClass('check true');
            $('#suscribirse_text').text('Sí');
        }else{
            $('#suscribirse').removeClass('check true');
            $('#suscribirse').addClass('check false');
            $('#suscribirse_text').text('No');
        }
    });
    $('#enviar').click((e)=>{
        e.preventDefault();
        var email = $('#email').val();
        if(email === ""){
            $('#email').addClass('input-invalid');
            $('#error').show();
        }else{
            post(
                '/referal',
                {'email':email, 'newsletter':suscribirse},
                (result,status)=>{
                    debugger;
                    if(status === success){
                        $('#landing').hide();
                        $('#leaving').show();
                    }else{
                        $('#error').text(result.responseText);
                        $('#error').show();
                    }
                },
                (result)=>{
                    $('#error').text(result.responseText);
                    $('#error').show();
                }
            )
        }
    });
    $('#leaving').hide();
});