const doc = document;

var DOM = function (object,document = doc) { 
    debugger;
    if (object === undefined || object === null || typeof object != typeof 'str' || object.length === 0) return;
    if (object.includes('#')) { // id
        return doc.getElementById(object);
    } else if (object.includes(".")) { // class
        return doc.getElementsByClassName(object);
    } else { // tag + selector
        if (object.includes("[") && object.includes("]")) {
            object = object.replaceAll('"', "'");
            var selectors = object.slice(object.indexOf("[")+1,object.indexOf("]")).split(',').filter( x => x.length > 0);
            if (selectors.length > 0) {
                var arr = [];
                var collection = doc.getElementsByTagName(object.slice(0, object.indexOf("[")));
                for (var i = 0; i < collection.length; i++){
                    var item = collection[i];
                    for (var j = 0; j < selectors.length; j++) {
                        if (selectors[j].includes('id')) { 
                            var searchId = selectors[j].splice('=');
                            if (item.id.includes(searchId)) arr.push(item);
                        }
                        else if (selectors[j].includes('class')) {
                            var searchClass = selectors[j].splice("=");
                            if (item.class.includes(searchClass)) arr.push(item);
                        } else { 
                            console.log('tagg');
                        }
                    }
                }
                return arr;
            }
            return collection;
        } else return doc.getElementsByTagName(object);
    }
}