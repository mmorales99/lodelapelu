const success = "success"

var black = "rgb(0, 0, 0)";
var white = "rgb(255, 255, 255)";

function selectValidation(id = '#select', errorId = '#error',validClass = 'input-valid',invalidClass = 'input-invalid'){
    function checkSelect(){
        var selection = $(this.id+' option:selected').val();
        if(selection === "0"){
            $(this).removeClass(validClass);
            $(this).addClass(invalidClass);
            $(errorId).show();
        }
        else{
            $(this).removeClass(invalidClass);
            $(this).addClass(validClass);
            $(errorId).hide();
        }
    }
    $(id).focusout(checkSelect);
    $(id).blur(checkSelect);
    $(id).change(checkSelect);
}

function emailValidation(id = '#email', errorId = '#error',validClass = 'input-valid',invalidClass = 'input-invalid'){
    function checkEmail(){
        var email = $(this).val();
        var empty = email == '';
        var valid = email.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
        if(empty || !valid){
            $(this).removeClass(validClass);
            $(this).addClass(invalidClass);
            $(errorId).text("Has introducido un email incorrecto.");
            $(errorId).show();
        }
        else{
            $(this).removeClass(invalidClass);
            $(this).addClass(validClass);
            $(errorId).hide();
        }
    }
    $(id).focusout(checkEmail);
    $(id).blur(checkEmail);
    $(id).change(checkEmail);
}

function multilineValidation(id = '.multiline-input', errorId = '#error',validClass = 'input-valid',invalidClass = 'input-invalid'){
    function checkMultiline(){
        debugger;
        var input = $(this).val();
        if(input !== "" || $(this).css("color") === white){
            $(this).addClass(validClass);
        }
        else{
            $(this).removeClass(validClass);
        }
    }
    $(id).focusout(checkMultiline);
    $(id).blur(checkMultiline);
    $(id).change(checkMultiline);
}

function hideHeader(){
    $('header').hide();
}

function post(destination,data,success_callback,error_callback,complete_callback,expected_dataType=''){
    $.ajax({
        url:destination,
        async:true,
        data:JSON.stringify(data),
        timeout:3000,
        type:'POST',
        dataType: expected_dataType==='' ? "json" : expected_dataType,
        contentType: "application/json",
        processData:false,
        success:success_callback,
        error:error_callback,
        complete:complete_callback
    });
}