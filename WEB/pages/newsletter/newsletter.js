/* JS de la pagina */
$((e)=>{
    hideHeader();
    emailValidation();
    $('#enviar').click((e)=>{
        e.preventDefault();
        var email = $('#email').val();
        if(email === ""){
            $('#email').addClass('input-invalid');
            $('#error').show();
        }else{
            post(
                '/newsletter',
                {'email':email, 'newsletter':true},
                (result,status)=>{
                    debugger;
                    if(status === success){
                        $('#landing').hide();
                        $('#leaving').show();
                    }else{
                        $('#error').text(result.responseText);
                        $('#error').show();
                    }
                },
                (result)=>{
                    $('#error').text(result.responseText);
                    $('#error').show();
                }
            )
        }
    });
    $('#leaving').hide();
});