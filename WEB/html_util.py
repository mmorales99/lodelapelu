from http import HTTPStatus
from fastapi.responses import Response, FileResponse
from os.path import join, exists
from pathlib import Path
import filetype

class html:
    # a cambiar por configuracion en un tml
    files_src = Path('.') / 'pages'
    html_base:str = ''
    serverurl:str = 'www.tintracker.es'

    def load(self) -> None:
        with open(self.files_src/'html_base'/'html_base.html',encoding='UTF-8') as f:
            self.html_base = f.read()
        self.html_base = self.html_base.replace('serverurl',self.serverurl)
        pass

    def __init__(self) -> None:
        self.load()
        pass

    def bind(self, page:str='', data:dict[str,str]={}):
        for k in data.keys():
            page = page.replace('${'+k+'}',data.get(k,''))
        return page

    def deliver_page(self,page_lrl:str = 'home',data:dict[str,str]=None) -> Response:
        try:
            self.load()
            section = ''
            with open(self.files_src/page_lrl/(page_lrl+'.html'),encoding='UTF-8') as f:
                section = f.read()
            if data is not None:
                section = self.bind(section,data)
            
            return Response(content=self.bind(self.html_base,{'page':section}), status_code=HTTPStatus.OK)
        except Exception as e:
            return Response(content=f'ERROR in deliver_page:\n'+e.__str__(),status_code=HTTPStatus.INTERNAL_SERVER_ERROR)

    def deliver_element(self,source:str='',file:str='') -> Response:
        try:
            content:str
            src = join(self.files_src,source,file)
            if filetype.is_image(src):
                return FileResponse(path=src,media_type='image/'+file.split('.')[-1],status_code=HTTPStatus.OK)
            else:
                with open(src,encoding='UTF-8') as f:
                    content = f.read()
                return Response(content=content,status_code=HTTPStatus.OK,media_type='text/plain' if not file.__contains__('.css') else 'text/css')
        except Exception as e:
            return Response(content=f'ERROR in deliver_element:\n'+e.__str__(),status_code=HTTPStatus.INTERNAL_SERVER_ERROR)
    
    def deliver_content(self, element:str=''):
        src = join(self.files_src,element)
        if exists(src):
            return self.deliver_page(element)
        src = join(self.files_src,element.split('.')[0],element)
        if exists(src):
            return self.deliver_element(element.split('.')[0],element)
        else:
            return self.deliver_page('error',{'error':'No se ha encontrado el recurso que estas buscando... Prueba con otra cosa.'})
