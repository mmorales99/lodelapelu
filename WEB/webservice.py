from fastapi import FastAPI, Request, status
from fastapi.responses import Response, RedirectResponse, JSONResponse
from pydantic import BaseModel
import requests
import json
from starlette.exceptions import HTTPException
from datetime import datetime
from html_util import html
import uvicorn


# a cambiar por un archivo de confirguracion
UsuarioApiUrl = 'api.tintracker.es'
robotsUrl = './robots.txt'
sitemapUrl = './sitemap.xml'
appModule="webservice:app"
host="0.0.0.0"
port=80
workers=1
app_dir="."
server_header=False
date_header=False
# ssl-keyfile=os.path.join('.','cert','key.pem')
# ssl-certfile=os.path.join('.','cert','cert.pem')

page = html()
app = FastAPI()

class Usuario(BaseModel):
    nombre:str|None = None
    contraseña:str|None = None
    email:str|None = None
    newsletter:bool|None = None

class StandardException(HTTPException):
    def __init__(self, webMessage, internalMessage:str='') -> None:
        super().__init__(
            status_code=status.HTTP_418_IM_A_TEAPOT,
            detail=webMessage,
            headers={'X-Error-internal-Message':internalMessage.replace('\n','') if internalMessage != '' else webMessage,
                'X-Error-web-Message':webMessage,                
                'X-Exception-Date': str(datetime.now())
                }
            )
        print(internalMessage)

    def genResponse(exception:'StandardException'):
        return Response(
                content=page.deliver_page('error',{'error':exception.detail}),
                status_code=exception.status_code,
                headers=exception.headers
            )
@app.exception_handler(StandardException)
async def StandardException_handler(request: Request, exception:StandardException):
    # exception.headers['X-Error-request-type'] = request.type if request.type else 'uknown'
    # exception.headers['X-Error-request-proxy'] = request.has_proxy() if request.has_proxy() else 'uknown'
    # exception.headers['X-Error-request-origing-host'] = request.origin_req_host if request.origin_req_host else 'uknown'
    # exception.headers['X-Error-request-host'] = request.host if request.host else 'uknown'
    # exception.headers['X-Error-request-method'] = request.get_method() if request.get_method() else 'uknown'
    # exception.headers['X-Error-request-destination'] = request.full_url if request.full_url else 'uknown'
    # exception.headers['X-Error-request-headers'] = request.headers if request.headers else 'uknown'
    # exception.headers['X-Error-request-data'] = request.data if request.data else 'uknown'
    return StandardException.genResponse(exception)
@app.exception_handler(HTTPException)
async def HTTPException_handler(request: Request, exception):
    return page.deliver_page('error',{'error':exception.detail})

## Start de la web
@app.get('/')
async def root():
    return page.deliver_page()

@app.get('/sitemap.xml')
async def sitemap():
    string = ''
    with open(sitemapUrl) as f:
        string = f.read()
    return Response(content = string,status_code=status.HTTP_200_OK)

@app.get('/robots.txt')
async def robots():
    string = ''
    with open(robotsUrl) as f:
        string = f.read()
    return Response(content = string,status_code=status.HTTP_200_OK)

## Muestra la primera pagina
@app.get('/home')
async def home():
    return page.deliver_page()

@app.get('/error')
async def return_error():
    return page.deliver_content('_error')

@app.get('/RESOURCES/{file}')
async def return_resource(file:str):
    return page.deliver_element('RESOURCES',file)

@app.post('/newsletter')
async def add_email_to_newsletter(usuario:Usuario):
    response = requests.post(url=UsuarioApiUrl+'/usuarios/newsletter',json=json.dumps(usuario.__dict__))
    return JSONResponse({'message':'correo añadido a la newsletter'},status_code=status.HTTP_200_OK)

@app.post('/referal')
async def add_referal(usuario:Usuario):
    response = requests.post(url=UsuarioApiUrl+'/usuarios/referal',json=json.dumps(usuario.__dict__))
    return JSONResponse({'message':'correo añadido al referal project y la newsletter'},status_code=status.HTTP_200_OK)

@app.get('/{element}')
async def return_content(element:str):
    return page.deliver_content(element)

@app.get('/{element}/{p:path}')
async def return_content(element:str,p:str):
    return RedirectResponse("/"+p)

if __name__ == "__main__":
    uvicorn.run(app=appModule,
                host=host,
                port=port,
                workers=workers,
                app_dir=app_dir,
                server_header=server_header,
                date_header=date_header
                )