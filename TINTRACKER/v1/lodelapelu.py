from doctest import REPORT_ONLY_FIRST_FAILURE
from email.errors import CloseBoundaryNotFoundDefect
from mailbox import NoSuchMailboxError
from msvcrt import getch
from os import system
from pickle import TRUE
from queue import Empty
import string

def load_key()->bytes:
    seckey = './bin/sec_key'
    key:bytes
    try:
        with open(seckey,'rb') as f:
            key = f.read()
    except:
        from cryptography.fernet import Fernet as crypto
        key = crypto.generate_key()
        try:
            from os import mkdir
            mkdir('./bin')
        except: pass
        with open(seckey,'wb') as fk:
            fk.write(key)
    return key

def encrypt_file(file):
    from cryptography.fernet import Fernet as crypto
    fernet = crypto(load_key())
    with open(file,'rb') as f:
        original = f.read()
    encrypted = fernet.encrypt(original)
    with open(file,'wb') as f:
        f.write(encrypted)
    pass

def encrypt_str2str(string:str):
    from cryptography.fernet import Fernet as crypto
    return crypto(load_key()).encrypt(string.encode()).decode()

def encrypt(string:str):
    from cryptography.fernet import Fernet as crypto
    return crypto(load_key()).encrypt(string.encode())


def decrypt_file(file):
    from cryptography.fernet import Fernet as crypto
    fernet = crypto(load_key())
    with open(file,'rb') as f:
        original = f.read()
    return fernet.decrypt(original)

def decrypt_str2str(string:str):
    from cryptography.fernet import Fernet as crypto
    return crypto(load_key()).decrypt(string.encode()).decode()

def decrypt(string:str):
    from cryptography.fernet import Fernet as crypto
    return crypto(load_key()).decrypt(string.encode())

def try_to_load() -> dict[str,str]:
    import json
    try:
        return json.loads(decrypt_file('./data/data'))
    except IOError:
        print('Ha habido un error con la base de datos, se va a hacer una nueva vacía.')
        import os
        if not os.path.isdir('./data/'):
            os.mkdir('./data/')
        with open('./data/data','xb') as f:
            f.write(encrypt('{}'))
        return {}
    except Exception as e :
        print('Ha habido un error interno, lo siento...')
        print(e)
        return {}

def menu() -> int:
    from os import system
    system('cls')
    print('')
    print('▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄')
    print('█▄▄░▄▄██▄██░▄▄▀█▄▄░▄▄█░▄▄▀█░▄▄▀█▀▄▀█░█▀█░▄▄█░▄▄▀██')
    print('███░████░▄█░██░███░███░▀▀▄█░▀▀░█░█▀█░▄▀█░▄▄█░▀▀▄██')
    print('███░███▄▄▄█▄██▄███░███▄█▄▄█▄██▄██▄██▄█▄█▄▄▄█▄█▄▄██')
    print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
    print('')
    print('MENU:')
    print('Buscar cliente --------------------------------- 1')
    print('Nuevo cliente ---------------------------------- 2')
    print('Eliminar cliente ------------------------------- 3')
    print('Guardar (No siempre se guardá automático) ------ 4')
    print('Contacto e información ------------------------- 9')
    print('Salir (se guardará todo automático) ------------ 99')
    print('')
    return input('Escribe el numero de la opción que quieres:\t ')

def formatKeys(dict:dict[str,str]):
    str = ''
    if len(dict.values()) < 1:
        return 'No hay nada en la base de datos...'
    max_key_length = len(max(dict, key=len))
    max_value_length = len(max(dict.values(),key=len))
    str = ' Nombre'+' '*(max_key_length - len('Nombre')+1)+'| Tinte y observaciones\n'
    hori = max_key_length + max_value_length + 4
    str = str + '-'*(hori) + '\n'
    for k in dict.keys():
        str = str + ' ' + k + ' '*(max_key_length - len(k)+1) + '| '+ dict.get(k) + '\n'
    return str

def input_error(dict:dict[str,str]):
    print('Esa opción no existe. Pulsa una tecla y vuelve a probar.\t')
    getch()
    return dict

def buscar_por_nombre(dict:dict[str,str]):
    nombre = ''
    n = input('A quién quieres buscar?(Pon salir y... pum sorpresa!)\t').strip().lower()
    if len(n) < 1:
        return nombre
    else:
        d1 = {}
        nsplit = n.split()
        for key in dict.keys():
            ksplit = key.lower().split()
            intersection = len(list(set(ksplit).intersection(nsplit)))
            union = (len(nsplit) + len(ksplit)) - intersection
            index = float(intersection)/union
            d1[key] = index
        total_elem = len(d1.values())
        l1 = list(d1.values())
        li = list(d1.keys())
        for i in range(total_elem-1):
            for j in range(total_elem-i-1):
                if l1[j] < l1[j+1]:
                    l1[j],l1[j+1] = l1[j+1],l1[j]
                    li[j],li[j+1] = li[j+1],li[j]
        ## indices del diccionario ordenados, falta mostrar por pantalla
        dd = {}
        for k in li:
            dd[k] = dict.get(k)
        system('cls')
        print('Estos son los mejores resultados de la busqueda:\n')
        print(formatKeys(dd))
        name = input('Escribe el nombre completo del que quieras seleccionar:\t')
    return name

def buscar_cliente(dict:dict[str,str]):
    from os import system
    system('cls')
    if len(dict.values()) < 1:
        input('No hay clientes en la base de datos... Pulsa una tecla para continuar')
        return dict
    print('Esto es cómo Google, pon el nombre de quién quieras buscar y yo te lo encontraré si lo tengo.')
    print('Te mostaré una lista de nombres que se parezcan al que me has pedido, escoge uno y podrás cambiar sus datos.')
    print('Si no contestas se enseñaré todos los que tengo.')
    nombre:str = buscar_por_nombre(dict)
    system('cls')
    if len(nombre) < 1:
        print(formatKeys(dict))
        while True:
            cual = input('Cual?(Pon el nombre completo, sino puede que borres el que no quieres)\t ')
            if dict.get(cual) == None:
                print('No has introducido un nombre válido. Vuelve a probar por favor.')
            else:
                break
    elif nombre.lower() == 'salir':
        return dict
    while True:
        cual = nombre
        system('cls')
        print('Aquí puedes modificar el nombre o el comentario/tinte.')
        print(f'Nombre:\t{cual}')
        print(f'Tinte:\t{dict.get(cual)}')
        print('')
        print('Pon el numero de la opcion que quieras realizar.')
        opt=input('Modificar - Nombre [1] - Tinte[2] - Salir [3] - Salir y guardar[4]:\t')
        if opt == '1':
            nombre = input('Escribe el nuevo nombre:\t')
            if cual != nombre:
                dict[nombre] = dict.get(cual)
                dict.pop(cual)
        elif opt=='2':
            tinte = input('Escribe el tinte aplicado o una nota, ten en cuenta que se borrará lo que tuviera apuntado:  ')
            dict[cual] = tinte
        elif opt=='3':
            break
        elif opt=='4':
            guardar_todo(dict)
            break
        else:
            opt = input('Esa opcion aún no existe, pulsa intro y vuelve a probar.')
    #print(dict.get(buscar_por_nombre(dict)))
    return dict

def nuevo_cliente(dict:dict[str,str]):
    from os import system
    system('cls')
    print('Creación de clientes:')
    while True:
        nombre = input('Nombre del cliente:\t ')
        if len(nombre) < 1:
            print('El nombre no puede estar vacio, sino podrías producir que todo el programa falle')
        cliente = dict.get(nombre)
        if cliente is not None:
            print('Ya hay un cliente registrado con ese nombre, prueba con otro.')
            continue
        else:
            dict[nombre] = input('Tinte aplicado:\t\t ')
            break
    return dict

def eliminar_cliente(dict:dict[str,str]):
    from os import system
    system('cls')
    print('Que cliente quieres borrar? ')
    opt = input('Ver todos - 1 || Buscar uno - 2 \t :')
    cual = ''
    if opt == '1':
        system('cls')
        print(formatKeys(dict))
        cual = input('Cual?(Pon el nombre completo, sino puede que borres el que no quieres)\t ')
    else:
        cual = buscar_por_nombre(dict)
        if len(cual) < 1:
            print('No puedo borrar nada si no me das un nombre.')

    if dict.get(cual) is not None:
            dict.pop(cual)
            print(f'Se ha borrado a {cual}. Ya no estará en la base de datos.')
    return dict

def guardar_todo(dict:dict[str,str]):
    import json
    with open('./data/data','wb') as f:
        f.write(encrypt(json.dumps(dict,ensure_ascii=False)))
    return dict

def info(dict):
    print('.-----------------------------------------.')
    print('|           T I N T R A C K E R           |')
    print(':-------------.---------------------------:')
    print('| Version     | 20.02.2022.0.1            |')
    print(':-------------+---------------------------:')
    print('| Publicador  | Manuel Juan Morales Amat  |')
    print(':-------------+---------------------------:')
    print('| Soporte     | mjmorales+dev@gmail.com   |')
    print(':-------------+---------------------------:')
    print('| Soporte Tlf | 673522260                 |')
    print('\'-------------\'---------------------------\'')
    return dict

def operaciones(opt:int):
    switch = {
        '1':buscar_cliente,
        '2':nuevo_cliente,
        '3':eliminar_cliente,
        '4':guardar_todo,
        '9':info
    }
    return switch.get(opt,input_error)

def main():
    dict = try_to_load()
    while(True):
        opt = menu()
        if opt == '99':
            guardar_todo(dict)
            print('Se ha guardado todo... Saliendo del programa...')
            break
        dict = operaciones(opt)(dict)
    exit(0)