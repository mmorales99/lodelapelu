from ctypes import windll
from logging import root
from tkinter import *
from tkinter import messagebox
import lodelapelu
import sys


def clear_holder(event, entry,texto):
    if type(entry) is Text:
        t = entry.get('1.0',END).strip(' \n\t\r')
        if t == texto:
            entry.delete('1.0', END) 
    elif type(entry) is Entry:
        t =entry.get() 
        if entry.get() == texto:
            entry.delete(0, END)

def write_holder(event, entry,texto):
    if type(entry) is Text:
        t = entry.get('1.0',END).strip(' \n\t\r')
        if t == '':
            entry.insert('1.0', texto) 
    elif type(entry) is Entry:
        if entry.get() == '':
            entry.insert(0, texto)

class Window(Frame):
    def __init__(self, master:Misc=None):
        Frame.__init__(self, master)
        self.master:Misc = master
        self.dict = lodelapelu.try_to_load()
        lb = self.lb = Listbox(self.master,height=20)
        xscroll = Scrollbar(self.master,orient=HORIZONTAL,command=lb.xview)
        xscroll.place(relwidth=0.33,relx=0.02,rely=0.93)
        yscroll = Scrollbar(self.master,orient=VERTICAL,command=lb.yview)
        yscroll.place(relheight=0.8,relx=0.35,rely=0.13)
        lb['xscrollcommand'] = xscroll.set
        lb['yscrollcommand'] = yscroll.set
        lbname = Label(text='Nombre de los clientes:')
        lbname.place(relx=0.02,rely=0.06)
        lb.place(relheight=0.8,relwidth=0.33,relx=0.02,rely=0.13)

        clienteNombre = Label(text='Nombre del cliente seleccionado:')
        clienteNombre.place(relx=0.39,rely=0.06)
        tiNombre = self.tiNombre = Entry(self.master,state='readonly')
        tiNombre.place(relwidth=0.55,relx=0.39,rely=0.13)
        clienteDetalle = Label(text='Detalle del cliente seleccionado:')
        clienteDetalle.place(relx=0.39,rely=0.23)
        tinTexto = self.tinTexto = Text(self.master,wrap=WORD,state=DISABLED)
        textScrollbar = Scrollbar(self.master,orient=VERTICAL,command=tinTexto.xview)
        tinTexto['yscrollcommand']=textScrollbar.set
        tinTexto.place(relheight=0.55,relwidth=0.55,relx=0.39,rely=0.3)
        textScrollbar.place(relheight=0.55,relx=0.9425,rely=0.3)

        cuadroBusqueda = Entry(self.master)
        cuadroBusqueda.place(rely=0.01,relx=0.01,relwidth=0.63)

        def buscar():
            tiNombre.configure(state='normal')
            tinTexto.configure(state='normal')
            guardarCambios_b.configure(state=ACTIVE)
            descartarCambios_b.configure(state=ACTIVE)
            tiNombre.delete(0,END)
            tinTexto.delete('1.0',END)
            n = cuadroBusqueda.get().lower()
            d1 = {}
            for key in lb.get(0,END):
                ksplit = key.lower()
                kindex = 0
                for kk in ksplit:
                    intersection = len(list(set(ksplit).intersection(n)))
                    union = (len(n) + len(ksplit)) - intersection
                    index = float(intersection)/union
                    kindex+=index
                d1[key] = kindex
            total_elem = len(d1.values())
            l1 = list(d1.values())
            li = list(d1.keys())
            for i in range(total_elem-1):
                for j in range(total_elem-i-1):
                    if l1[j] < l1[j+1]:
                        l1[j],l1[j+1] = l1[j+1],l1[j]
                        li[j],li[j+1] = li[j+1],li[j]
            tiNombre.insert(0,li[0])
            tinTexto.insert('1.0',self.dict.get(li[0]))
            lb.delete(0,END)
            c = 1
            for k in li:
                lb.insert(c,k)
                c+=1
        buscarBoton = Button(self.master,text='Buscar persona',command=buscar)
        buscarBoton.place(rely=0.005,relx=0.65,relwidth=0.3)
        
        texto = 'Pon un nombre y dale a Buscar persona'
        cuadroBusqueda.insert(0,texto)
        cuadroBusqueda.bind('<Leave>',lambda e:write_holder(e,cuadroBusqueda,texto))
        cuadroBusqueda.bind('<Enter>',lambda e:clear_holder(e,cuadroBusqueda,texto))
        cuadroBusqueda.bind('<Return>',lambda e:buscar())

        #queda guardar y borrar

        def nuevoCliente():
            tiNombre.configure(state='normal')
            tinTexto.configure(state='normal')
            tiNombre.delete(0,END)
            tinTexto.delete('1.0',END)
            texto1 = 'Escribe aquí algun comentario y pulsa en Guardar o en Descartar'
            texto2 = 'Escribe aquí el nombre'
            tiNombre.insert(0,texto2)
            tinTexto.insert('1.0',texto1)
            tinTexto.bind('<Leave>',lambda e:write_holder(e,tinTexto,texto1))
            tinTexto.bind('<Enter>',lambda e:clear_holder(e,tinTexto,texto1))
            tiNombre.bind('<Leave>',lambda e:write_holder(e,tiNombre,texto2))
            tiNombre.bind('<Enter>',lambda e:clear_holder(e,tiNombre,texto2))
            guardarCambios_b.configure(state=ACTIVE)
            descartarCambios_b.configure(state=ACTIVE)
            pass
        menu = self.menu = Menu(self.master)
        self.master.config(menu=menu)
        menu.add_command(label='Nuevo cliente',command=nuevoCliente)
        
        def guardar():
            if self.dict.get(tiNombre.get()) != None:
                confirm = messagebox.askyesno(title='TINTRAKER-Seguridad',master=self.master,message='Ya hay alguien con ese nombre. ¿Quieres borrarlo y vovler a hacerlo de nuevo?', icon='warning')
                if confirm == False:
                    return
                else:
                    i = lb.get(0,END).index(tiNombre.get())
                    lb.delete(i)
            self.dict[tiNombre.get()] = tinTexto.get('1.0',END) 
            lb.insert(c,tiNombre.get())
            tiNombre.delete(0,END)
            tinTexto.delete('1.0',END)
            guardarCambios_b.configure(state=DISABLED)
            descartarCambios_b.configure(state=DISABLED)
            tinTexto.unbind('<Leave>')
            tinTexto.unbind('<Enter>')
            tiNombre.unbind('<Leave>')
            tiNombre.unbind('<Enter>')
            tiNombre.configure(state=DISABLED)
            tinTexto.configure(state=DISABLED)
            pass
        guardarCambios_b = Button(self.master,text='Guardar cambios',command=guardar,state=DISABLED)
        guardarCambios_b.place(relheight=0.1,relwidth=0.25,relx=0.41,rely=0.87)
        
        def descartar():
            tiNombre.delete(0,END)
            tinTexto.delete('1.0',END)
            guardarCambios_b.configure(state=DISABLED)
            descartarCambios_b.configure(state=DISABLED)
            tiNombre.configure(state=DISABLED)
            tinTexto.configure(state=DISABLED)
        descartarCambios_b = Button(self.master,text='Descartar cambios',command=descartar,state=DISABLED)
        descartarCambios_b.place(relheight=0.1,relwidth=0.25,relx=0.66,rely=0.87)

        def eliminarCliente():
            n = tiNombre.get()
            if self.dict.get(n) != None:
                self.dict.pop(n)
                c=0
                for i in lb.get(0,END):
                    if i == n:
                        lb.delete(c)
                        break
                    c+=1
            tinTexto.delete('1.0',END)
            tiNombre.delete(0,END)
            guardarCambios_b.configure(state=DISABLED)
            descartarCambios_b.configure(state=DISABLED)
            tinTexto.configure(state=DISABLED)
            tiNombre.configure(state=DISABLED)
            pass
        menu.add_command(label='Eliminar cliente',command=eliminarCliente)
        menu.add_command(label='Guardar',command=lambda: self.guardar())
        menu.add_command(label='Guardar y Salir',command=self.saveNexit)
        def informacion():
            messagebox.showinfo("Información acerca de la aplicación", 
                '           T I N T R A C K E R\n'+
                'Version: 20.02.2022.0.1\n'+
                'Publicador: Manuel Juan Morales Amat\n'+
                'Soporte: mjmorales+dev@gmail.com\n'+
                'Soporte Tlf: 673522260'
            )
        menu.add_command(label='Acerca del programa',command=informacion)

        c = 1
        for k in self.dict.keys():
            lb.insert(c,k)
            c+=1
        def show_person_data(select):
            tiNombre.configure(state='normal')
            tinTexto.configure(state='normal')
            n = lb.get(select[0])
            tiNombre.delete(0,END)
            tiNombre.insert(0,n)
            t = self.dict[n]
            tinTexto.delete('1.0',END)
            tinTexto.insert('1.0',t)
            guardarCambios_b.configure(state=ACTIVE)
            descartarCambios_b.configure(state=ACTIVE)

        lb.bind('<<ListboxSelect>>',lambda e: show_person_data(lb.curselection()))

        pass
    
    def saveNexit(self):
        self.guardar(self)
        exit()

    def guardar(self):
         lodelapelu.guardar_todo(self.dict)

def main():
    root = Tk()
    app = Window(root)
    root.wm_title('TINTRACKER')
    root.geometry("625x460")
    root.mainloop()
    pass


#########################################
if __name__ == '__main__':
    if sys.argv.__contains__('terminal'):
        lodelapelu.main()
        pass
    else:
        main()