class Cliente:
    _id:str
    _nombre:str

    def __init__(self, name:str = '') -> None:
        self._nombre = name

    @property
    def nombre(self):
        return self._nombre

    @property
    def id(self):
        return self._id