def Null(par = None) -> bool:
    if(type(par) == type(str)):
        return par == None
    else:
        return False

def Empty(par = None) -> bool:
    if(type(par) == type(str) and not Null(par)):
        return len(par) > 0 
    else:
        return False

def NullOrEmpty(par = None) -> bool:
    return Null(par) or Empty()