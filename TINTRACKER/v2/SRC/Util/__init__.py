from .comun import Null, Empty, NullOrEmpty

__all__ = [
    'Null',
    'Empty',
    'NullOrEmpty',
]