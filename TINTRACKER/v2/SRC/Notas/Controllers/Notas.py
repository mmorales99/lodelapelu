from Models import Notas as MNotas
from Util import *

class Notas(MNotas):
    def nuevaNota(contenido:str) -> MNotas:
        return MNotas(contenido)

    def darContenido(self,contenido:str) -> bool:
        if not NullOrEmpty(self._contenido):
            self.quitarContenido()
        self._contenido = contenido
        return True
    
    def quitarContenido(self) -> bool:
        del self._contenido
        if not NullOrEmpty(self._contenido):
            return False
        return True