class Notas:
    _contenido:str

    @property
    def notas(self):
        return self._contenido

    def __init__(self, contenido:str = '') -> None:
        self._contenido = contenido
        pass