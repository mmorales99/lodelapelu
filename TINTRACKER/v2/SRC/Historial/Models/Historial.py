import Cliente
import Notas
import Fecha

class Historial:
    _cliente:Cliente
    _lista_notas:dict(Fecha,Notas)

    @property
    def cliente(self):
        return self._cliente

    @property
    def lista_notas(self):
        return self._lista_notas