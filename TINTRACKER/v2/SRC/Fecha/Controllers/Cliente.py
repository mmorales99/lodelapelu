from Models import Cliente as MCliente
from Util import *

class Cliente(MCliente):
    def nuevoCliente(nombre:str) -> MCliente:
        if not NullOrEmpty(nombre):
            c:MCliente = MCliente(nombre)
            return c
        return MCliente()

    def darNombre(self,nombre:str) -> bool:
        if not NullOrEmpty(nombre):
            if not NullOrEmpty(self._nombre):
                self.quitarNombre()
            self._nombre = nombre
            return True
        return False
    
    def quitarNombre(self) -> bool:
        del self._nombre
        if not NullOrEmpty(self._nombre):
            return False
        return True